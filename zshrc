# vi: set ft=zsh :

###
# .zshrc is called for interactive shells (sourced after .zprofile).
###

# Set PATH, MANPATH, etc., for Homebrew.
eval "$(/opt/homebrew/bin/brew shellenv)"
alias ls='\ls --color -FG'

# Customise zsh prompt.
eval "$(starship init zsh)"

# Added by GDK bootstrap
source /Users/erran/.asdf/asdf.sh

type brew &>/dev/null && FPATH="$(brew --prefix)/share/zsh/site-functions:${FPATH}"
fpath=(${ASDF_DIR}/completions $fpath)

autoload -z compinit && compinit

# Added by Docker Desktop
source /Users/erran/.docker/init-zsh.sh || true
