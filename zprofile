# vi: set ft=zsh :

###
# .zprofile is called for login shells (sourced before .zshrc).
###

# Enable gpg to prompt for password.
export GPG_TTY=$(tty)
