set backspace=indent,eol,start
set expandtab
set number
set numberwidth=1
set relativenumber
set ruler
set shiftwidth=2
set tabstop=2
set wildmenu
set wildmode=list:longest,full
syntax on

"vim -u NONE -c "helptags ~/.vim/pack/vendor/start/nerdtree/doc" -c q
nnoremap <leader>n :NERDTreeToggle<CR>
" Start NERDTree when Vim is started without file arguments.
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists('s:std_in') | NERDTree | endif

command! GoFmt write|silent execute '!go fmt % && gosimports -w %'|edit|redraw!

" Highlight matches for /searchResults
set hlsearch
hi Search cterm=NONE ctermbg=NONE ctermfg=green

" Setup :Ag as a command and highlight the selected line.
set grepprg=ag\ --nogroup\ --nocolor
nnoremap K :grep! "\b<C-R><C-W>\b"<CR>:cw<CR>
command -nargs=+ -complete=file -bar Ag silent! grep! <args>|cwindow|redraw!
hi QuickFixLine term=underline ctermfg=6 guifg=DarkCyan
